FROM alpine

RUN sed -i 's/https/http/' /etc/apk/repositories # FIXME
RUN apk add \
    --no-cache --update \
    --repository http://dl-cdn.alpinelinux.org/alpine/edge/main \
    openvpn iptables openvpn-auth-ldap bash bind-tools net-tools curl

# ENTRYPOINT mkdir -p /dev/net && mknod /dev/net/tun c 10 200 && openvpn $OPENVPN_OPTS
ENTRYPOINT openvpn $OPENVPN_OPTS
